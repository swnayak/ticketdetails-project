﻿using Library;
using System;
using System.IO;
using System.Configuration;

namespace MsPatching
{
    class Program
    {
        static void Main(string[] args)
        {
            string logPath = Path.GetFullPath(ConfigurationManager.AppSettings["Log"].ToString());
            Log.write("/*****************MS Patching Scheduler execution starts at " + DateTime.Now.ToString() + "*****************/", null, logPath);
            string conStr = ConfigurationManager.AppSettings["XlsxConn"].ToString();
            string directory = Path.GetFullPath(ConfigurationManager.AppSettings["FilePath"].ToString());

            if (!string.IsNullOrEmpty(conStr) && !string.IsNullOrEmpty(directory))
            {
                MsPatchingHelper.logPath = logPath;
                MsPatchingHelper.ExecuteMSPatchingReminders(conStr, directory);
            }
            else
            {
                Log.write("Either XlsxConn or FilePath keys are empty in the config file, thus existing the application ( No mails sent)", null, logPath);
            }
            Log.write("/*****************MS Patching Scheduler execution ends at" + DateTime.Now.ToString() + "*****************/", null, logPath);
        }
    }
}
