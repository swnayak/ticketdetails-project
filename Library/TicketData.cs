﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Linq;

namespace Library
{
    public class TicketData
    {
        readonly private string ConnectionString;
        readonly private string MasterConnectionString;
        public string MailTo;
        public string MailCc;
        public string IncidentMailSubject;
        public string ServiceRequestMailSubject;
        public string ProblemMailSubject;
        public string LogPath;

        //master tables
        private DataTable dtSolutions;
        private DataTable dtAgeGroup;
        private DataTable dtResponseSLA;
        private DataTable dtServiceArea;
        private DataTable dtSogetiResolutionTime;
        private DataTable dtExcel;

        //report tables
        private DataTable dtOverAllActiveIncidnet;
        private DataTable dtOverAllActiveServiceRequets;
        private DataTable dtOverAllActiveProblem;
        private DataTable dtAgeingAcceptedIncidnet;
        private DataTable dtAgeingAcceptedServiceRequets;
        private DataTable dtAgeingAcceptedProblem;
        private DataTable dtAgeingWaitingIncidents_One;
        private DataTable dtAgeingWaitingServiceRequest_One;
        private DataTable dtAgeingWaitingProblem_One;
        private DataTable dtAgeingWaitingIncidents_Two;
        private DataTable dtAgeingWaitingServiceRequest_Two;
        private DataTable dtAgeingWaitingProblem_Two;


        private const string Ticket = "TICKET_DATA";
        private const string Solution = "MASTER_SOLUTION";
        private const string AgeGroup = "MASTER_AGEGROUP";
        private const string ResponseSLA = "MASTER_RESPONSESLA";
        private const string ServiceArea = "MASTER_SERVICEAREA";
        private const string SogetiResolutionTime = "MASTER_SOGETIRESOLUTIONTIME";




        public TicketData(string connectionstring, string masterConnectionString)
        {
            ConnectionString = connectionstring;
            MasterConnectionString = masterConnectionString;
        }

        public void LoadMasterData()
        {
            dtSolutions = PopulateDataTable(MasterConnectionString, 0, Solution);
            dtAgeGroup = PopulateDataTable(MasterConnectionString, 1, AgeGroup);
            dtResponseSLA = PopulateDataTable(MasterConnectionString, 2, ResponseSLA);
            dtServiceArea = PopulateDataTable(MasterConnectionString, 3, ServiceArea);
            dtSogetiResolutionTime = PopulateDataTable(MasterConnectionString, 4, SogetiResolutionTime);
        }

        public void Process()
        {
            try
            {
                Email.ticketStatusLogPath = LogPath;
                Log.write("Populating data table", null, LogPath);
                dtExcel = PopulateDataTable(ConnectionString, 0, Ticket);
                if (dtExcel.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtExcel.Rows)
                    {
                        dr["CompletedMonth"] = SetCompletedMonth(Convert.ToString(dr["Resolved On"]));
                        dr["ProductName"] = SetProductName(Convert.ToString(dr["Service-Solution"]));
                        dr["Age_Days"] = SetAgeDays(Convert.ToString(dr["Created On"]));
                        dr["AgeGroup"] = SetAgeGroup(Convert.ToString(dr["Age_Days"]));
                        dr["ResponseTime_Hrs"] = SetResponseTimeHrs(Convert.ToString(dr["Assigned On"]), Convert.ToString(dr["Created On"]));
                        dr["ResponseSLAMet"] = SetResponseSLAMet(Convert.ToString(dr["Priority (Task)"]), Convert.ToString(dr["Parent Object"]), Convert.ToString(dr["ResponseTime_Hrs"]));
                        dr["ResolutionTime_Hrs"] = SetResolutionTimeHrs(Convert.ToString(dr["Resolved On"]), Convert.ToString(dr["Assigned On"]), Convert.ToString(dr["Created On"]));
                        dr["SogetiResolutionTime_Hrs"] = SetSogetiResolutionTimeHrs(Convert.ToString(dr["ResolutionTime_Hrs"]), Convert.ToString(dr["Total Effort"]));
                        dr["ResolutionTimeSLAMet"] = SetResolutionTimeSLAMet(Convert.ToString(dr["Priority (Task)"]), Convert.ToString(dr["Parent Object"]), Convert.ToString(dr["SogetiResolutionTime_Hrs"]), Convert.ToString(dr["Assigned On"]));
                        dr["ServiceArea"] = SetServiceArea(Convert.ToString(dr["ProductName"]));
                        dr["BlackAlert"] = SetBlackAlert();
                        dr["CreatedMonth"] = SetCreatedMonth(Convert.ToString(dr["Created On"]));
                    }
                }

                Log.write("Generating reports data table", null, LogPath);
                GenerateReports();

                Log.write("Sending Emails", null, LogPath);
                SendEmail();
            }
            catch (Exception ex)
            {
                Log.write("ERrOR:  ", ex, LogPath);
            }

        }


        #region private methods

        private void SendEmail()
        {
            Email.SendEmailForIncidents(dtOverAllActiveIncidnet, dtAgeingAcceptedIncidnet, dtAgeingWaitingIncidents_One, dtAgeingWaitingIncidents_Two, IncidentMailSubject, MailTo, MailCc);
            Email.SendEmailForServiceRequest(dtOverAllActiveServiceRequets, dtAgeingAcceptedServiceRequets, dtAgeingWaitingServiceRequest_One, dtAgeingWaitingServiceRequest_Two, ServiceRequestMailSubject, MailTo, MailCc);
            Email.SendEmailForProblem(dtOverAllActiveProblem, dtAgeingAcceptedProblem, dtAgeingWaitingProblem_One, dtAgeingWaitingProblem_Two, ProblemMailSubject, MailTo, MailCc);
        }

        private void GenerateReports()
        {
            OverAllStatusReport();
            AgeingAcceptedReport();
            AgeingWaitingReport();

        }

        private void AgeingWaitingReport()
        {
            AgeingWaitingStatusReport objAgeingWaitingStatus = new AgeingWaitingStatusReport();
            objAgeingWaitingStatus.GenerateAgeingWaitingStatusReport_One(dtSolutions, dtExcel, out dtAgeingWaitingIncidents_One, out dtAgeingWaitingServiceRequest_One, out dtAgeingWaitingProblem_One);
            objAgeingWaitingStatus.GenerateAgeingWaitingStatusReport_Two(dtSolutions, dtExcel, out dtAgeingWaitingIncidents_Two, out dtAgeingWaitingServiceRequest_Two, out dtAgeingWaitingProblem_Two);

        }

        private void AgeingAcceptedReport()
        {
            new AgeingAcceptedStatusReport().GenerateAgeingAcceptedStatusReport(dtSolutions, dtExcel, out dtAgeingAcceptedIncidnet, out dtAgeingAcceptedServiceRequets, out dtAgeingAcceptedProblem);
        }

        private void OverAllStatusReport()
        {
            new OverAllStatusReport().GenerateOverAllStatusReport(dtSolutions, dtExcel, out dtOverAllActiveIncidnet, out dtOverAllActiveServiceRequets, out dtOverAllActiveProblem);
        }

        private string SetCompletedMonth(string CompletedOn)
        {
            string CompletedMonth = string.IsNullOrEmpty(CompletedOn) ? "" : Convert.ToDateTime(CompletedOn).ToString("MMM-yy");
            return CompletedMonth;
        }

        private string SetCreatedMonth(string CreatedOn)
        {
            string CreatedMonth = string.IsNullOrEmpty(CreatedOn) ? "" : Convert.ToDateTime(CreatedOn).ToString("MMM-yy");
            return CreatedMonth;
        }

        private string SetBlackAlert()
        {
            string BlackAlert = "N";
            return BlackAlert;
        }

        private string SetServiceArea(string solution)
        {
            string ServiceArea = "";

            DataRow[] result = dtServiceArea.Select("Mapping = '" + solution + "'");
            if (!result.Any())
                return ServiceArea;

            ServiceArea = (result[0].ItemArray[1]).ToString();

            return ServiceArea;
        }

        private string SetResolutionTimeHrs(string completedOn, string firstAcceptedByCurrentTeam, string firstAssignedToCurrentTeam)
        {
            string SetResolutionTimeHrs = "";

            if ((string.IsNullOrEmpty(firstAcceptedByCurrentTeam) || string.IsNullOrEmpty(firstAssignedToCurrentTeam) || string.IsNullOrEmpty(completedOn)))
                return SetResolutionTimeHrs;
            DateTime fcell, xcell, ycell;

            if ((!DateTime.TryParse(firstAssignedToCurrentTeam, out ycell)) || (!DateTime.TryParse(completedOn, out fcell)) || (!DateTime.TryParse(firstAcceptedByCurrentTeam, out xcell)))
                return "0";


            if (ycell == fcell)
                return Math.Abs((Math.Round(24 * (fcell.Subtract(ycell).TotalDays), 2, MidpointRounding.AwayFromZero))).ToString();
            else
            {
                double DayEnd_DayStart = 0;
                string[] Start;
                string[] End;
                DateTime dtStart = DateTime.Now;
                DateTime dtEnd = DateTime.Now;

                foreach (DataRow dr in dtSogetiResolutionTime.Rows)
                {
                    dtStart = Convert.ToDateTime(dr[0]);
                    dtEnd = Convert.ToDateTime(dr[1]);
                }
                DayEnd_DayStart = Convert.ToDouble(Decimal.Divide(dtEnd.Subtract(dtStart).Hours, 24));
                Start = dtStart.ToString("H:mm").Split(':');
                End = dtEnd.ToString("H:mm").Split(':');

                TimeSpan tsStart = new TimeSpan(Convert.ToInt32(Start[0]), Convert.ToInt32(Start[1]), 0);
                TimeSpan tsEnd = new TimeSpan(Convert.ToInt32(End[0]), Convert.ToInt32(End[1]), 0);


                double val = 24 * DayEnd_DayStart * (
                        Math.Max(Math.Round(GetBusinessDays(ycell.AddDays(1), fcell.AddDays(-1)), 0, MidpointRounding.AwayFromZero), 0) +
                        (Math.Round(24 * ((fcell.Subtract(fcell.Date).TotalDays) - (ycell.Subtract(ycell.Date).TotalDays) + DayEnd_DayStart) / (24 * DayEnd_DayStart), 2, MidpointRounding.AwayFromZero))
                    )
                + (
                    Math.Round(((24 * (fcell.Subtract(fcell.Date).TotalDays)) - (24 * (tsStart.TotalDays))) + ((24 * (tsEnd.TotalDays)) - (24 * (ycell.Subtract(ycell.Date).TotalDays))), 2, MidpointRounding.AwayFromZero)
                    %
                    Math.Round((24 * (DayEnd_DayStart)), 2, MidpointRounding.AwayFromZero)
                    )
                ;

                SetResolutionTimeHrs = Math.Abs(val).ToString();
            }

            return SetResolutionTimeHrs;
        }

        private static double GetBusinessDays(DateTime startD, DateTime endD)
        {
            double calcBusinessDays =
                1 + ((endD - startD).TotalDays * 5 -
                (startD.DayOfWeek - endD.DayOfWeek) * 2) / 7;

            if (endD.DayOfWeek == DayOfWeek.Saturday) calcBusinessDays--;
            if (startD.DayOfWeek == DayOfWeek.Sunday) calcBusinessDays--;

            return calcBusinessDays;
        }

        private string SetResolutionTimeSLAMet(string priority, string parentObject, string SogetiResolutionTime_Hrs, string firstAssigned)
        {
            string ResolutionTimeSLA = "";
            if (string.IsNullOrEmpty(SogetiResolutionTime_Hrs) || string.IsNullOrEmpty(firstAssigned))
                ResolutionTimeSLA = "FIELD NOT POPULATED";
            else if (priority == "Low" || priority == "Medium")
                ResolutionTimeSLA = "NA";
            else if (parentObject == "Incident" || parentObject == "Service Request")
            {
                DataRow[] result = dtResponseSLA.Select("Priority = '" + priority + "'");
                if (!result.Any())
                    return ResolutionTimeSLA;

                if (Convert.ToDecimal((result[0]).ItemArray[2]) < Convert.ToDecimal(SogetiResolutionTime_Hrs))
                    ResolutionTimeSLA = "Resolution SLA Breached";
                else
                    ResolutionTimeSLA = "Resolution SLA Met";
            }

            return ResolutionTimeSLA;
        }

        private string SetAgeGroup(string ageDays)
        {
            string AgeGroup = "Completed";
            if (string.IsNullOrEmpty(ageDays))
                return AgeGroup;
            bool flag = false;
            int age = Convert.ToInt32(Convert.ToDecimal(ageDays));
            foreach (DataRow dr in dtAgeGroup.Rows)
            {
                if (age < Convert.ToInt32(dr[0]))
                {
                    flag = true;
                    AgeGroup = dr[1].ToString();
                    break;
                }
            }
            return flag ? AgeGroup : ">= 30 days";
        }

        private string SetResponseTimeHrs(string firstAcceptedByCurrentTeam, string firstAssignedToCurrentTeam)
        {
            string SetResponseTimeHrs = "";

            if ((string.IsNullOrEmpty(firstAcceptedByCurrentTeam)) || (string.IsNullOrEmpty(firstAssignedToCurrentTeam)))
                return SetResponseTimeHrs;

            DateTime xcell, ycell;

            if ((!DateTime.TryParse(firstAssignedToCurrentTeam, out ycell)) || (!DateTime.TryParse(firstAcceptedByCurrentTeam, out xcell)))
                return "0";


            if (ycell == xcell)
                return Math.Abs((Math.Round(24 * (ycell.Subtract(xcell).TotalDays), 2, MidpointRounding.AwayFromZero))).ToString();
            else
            {
                double DayEnd_DayStart = 0;
                string[] Start;
                string[] End;
                DateTime dtStart = DateTime.Now;
                DateTime dtEnd = DateTime.Now;

                foreach (DataRow dr in dtSogetiResolutionTime.Rows)
                {
                    dtStart = Convert.ToDateTime(dr[0]);
                    dtEnd = Convert.ToDateTime(dr[1]);
                }
                DayEnd_DayStart = Convert.ToDouble(Decimal.Divide(dtEnd.Subtract(dtStart).Hours, 24));
                Start = dtStart.ToString("H:mm").Split(':');
                End = dtEnd.ToString("H:mm").Split(':');

                TimeSpan tsStart = new TimeSpan(Convert.ToInt32(Start[0]), Convert.ToInt32(Start[1]), 0);
                TimeSpan tsEnd = new TimeSpan(Convert.ToInt32(End[0]), Convert.ToInt32(End[1]), 0);


                double val =
                    24 * DayEnd_DayStart * (
                        Math.Max(Math.Round(GetBusinessDays(ycell.AddDays(1), xcell.AddDays(-1)), 0, MidpointRounding.AwayFromZero), 0) +
                        (Math.Round(24 * ((xcell.Subtract(xcell.Date).TotalDays) - (ycell.Subtract(ycell.Date).TotalDays) + DayEnd_DayStart) / (24 * DayEnd_DayStart), 2, MidpointRounding.AwayFromZero))
                    )
                +
                (
                    Math.Round(((24 * (xcell.Subtract(xcell.Date).TotalDays)) - (24 * (tsStart.TotalDays))) + ((24 * (tsEnd.TotalDays)) - (24 * (ycell.Subtract(ycell.Date).TotalDays))), 2, MidpointRounding.AwayFromZero)
                    %
                    Math.Round((24 * (DayEnd_DayStart)), 2, MidpointRounding.AwayFromZero)
                    );

                SetResponseTimeHrs = Math.Abs(val).ToString();
            }
            return SetResponseTimeHrs;
        }

        private string SetSogetiResolutionTimeHrs(string ResolutionTimeHrs, string TotalWaitingDurationDays)
        {
            string SogetiResolutionTimeHrs = "";

            if (string.IsNullOrEmpty(ResolutionTimeHrs))
                return SogetiResolutionTimeHrs;

            DateTime dtStart = DateTime.Now;
            DateTime dtEnd = DateTime.Now;

            foreach (DataRow dr in dtSogetiResolutionTime.Rows)
            {
                dtStart = Convert.ToDateTime(dr[0]);
                dtEnd = Convert.ToDateTime(dr[1]);
            }

            SogetiResolutionTimeHrs = (Decimal.Parse(ResolutionTimeHrs, System.Globalization.NumberStyles.Any) - (Convert.ToDecimal(TotalWaitingDurationDays) * Decimal.Divide(dtEnd.Subtract(dtStart).Hours, 24))).ToString();

            return SogetiResolutionTimeHrs;
        }

        private string SetResponseSLAMet(string priority, string parentObject, string responseTimeHrs)
        {
            string ResponseSLA = "";

            if (string.IsNullOrEmpty(responseTimeHrs))
                ResponseSLA = "FIELD NOT POPULATED";
            else if (parentObject == "Incident" || parentObject == "ServiceReq")
            {
                DataRow[] result = dtResponseSLA.Select("Priority = '" + priority + "'");
                if (!result.Any())
                    return ResponseSLA;

                if (Convert.ToDecimal((result[0]).ItemArray[1]) < Convert.ToDecimal(responseTimeHrs))
                    ResponseSLA = "Response SLA Breached";
                else
                    ResponseSLA = "Response SLA Met";
            }
            else
                ResponseSLA = "NA";

            return ResponseSLA;
        }

        private string SetAgeDays(string CreatedOn)
        {
            string totalDays = "";

            if (string.IsNullOrEmpty(CreatedOn))
                return totalDays;

            DateTime currentDate = DateTime.Now;
            DateTime createdDate = Convert.ToDateTime(CreatedOn);
            totalDays = (((currentDate - createdDate).TotalDays) + 1).ToString();
            return totalDays;
        }

        private string SetProductName(string Solution)
        {

            DataRow[] result = dtSolutions.Select("ProductMapping = '" + Solution + "'");
            if (!result.Any())
                return "";

            return result[0][1].ToString();
        }

        private DataTable ReadExcelSheet(string sheetName, OleDbCommand cmdExcel, OleDbConnection connExcel, string identifier)
        {
            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            connExcel.Open();
            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
            oda.SelectCommand = cmdExcel;
            oda.Fill(dt);
            dt = ProcessIdentifer(dt, identifier);
            connExcel.Close();
            return dt;
        }

        private DataTable ProcessIdentifer(DataTable dt, string identifier)
        {
            switch (identifier)
            {
                case Ticket:
                    dt.Columns.Add("CompletedMonth");
                    dt.Columns.Add("ProductName");
                    dt.Columns.Add("Age_Days");
                    dt.Columns.Add("AgeGroup");
                    dt.Columns.Add("ResponseTime_Hrs");
                    dt.Columns.Add("ResponseSLAMet");
                    dt.Columns.Add("ResolutionTime_Hrs");
                    dt.Columns.Add("SogetiResolutionTime_Hrs");
                    dt.Columns.Add("ResolutionTimeSLAMet");
                    dt.Columns.Add("ServiceArea");
                    dt.Columns.Add("BlackAlert");
                    dt.Columns.Add("CreatedMonth");
                    break;
                case Solution:
                    break;
                case ResponseSLA:
                    break;
                case AgeGroup:
                    break;
                case ServiceArea:
                    break;
                case SogetiResolutionTime:
                    break;
            }
            return dt;
        }

        private string GetSheetName(int sheetIndex, OleDbCommand cmdExcel, OleDbConnection connExcel)
        {
            connExcel.Open();
            DataTable dtExcelSchema;
            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string SheetName = dtExcelSchema.Rows[sheetIndex]["TABLE_NAME"].ToString();
            connExcel.Close();
            return SheetName;
        }

        private DataTable PopulateDataTable(string connection, int sheetIndex, string identifier)
        {
            OleDbConnection connExcel = new OleDbConnection(connection);
            OleDbCommand cmdExcel = new OleDbCommand();

            DataTable dt = new DataTable();

            cmdExcel.Connection = connExcel;

            //Get the name of First Sheet
            string SheetName = GetSheetName(sheetIndex, cmdExcel, connExcel);

            //Read Data from Sheet
            dt = ReadExcelSheet(SheetName, cmdExcel, connExcel, identifier);


            return dt;
        }

        #endregion




    }
}
