﻿using System;
using System.Data;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Security.Policy;
using System.Net.Mail;
using System.Net.Mime;

namespace Library
{
    public class SolutionSpocs
    {
        public string SolutionName { get; set; }
        public string SpocName { get; set; }
        public SolutionSpocs(string solution, string spoc)
        {
            SolutionName = solution;
            SpocName = spoc;
        }
    }
    public static class Email
    {
        public const string MatchEmailPattern =
                    @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
             + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				        [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				        [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
             + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        public static string MsPatchingLogPath;
        public static string ticketStatusLogPath;
        public static string NugetPatchingLogPath;
        public static string CCBMailLogPath;
        static string sharePointUrl = ConfigurationManager.AppSettings["SharepointURL"].ToString();
        public static void SendEmailForIncidents(DataTable dtActive, DataTable dtAgeingAccepted, DataTable dtWaiting_One, DataTable dtWaiting_Two, string mailSubject, string mailTo, string mailCc)
        {
            SendEmail(dtActive, dtAgeingAccepted, dtWaiting_One, dtWaiting_Two, mailSubject, mailTo, mailCc, "Incident");
        }

        public static void SendEmailForServiceRequest(DataTable dtActive, DataTable dtAgeingAccepted, DataTable dtWaiting_One, DataTable dtWaiting_Two, string mailSubject, string mailTo, string mailCc)
        {
            SendEmail(dtActive, dtAgeingAccepted, dtWaiting_One, dtWaiting_Two, mailSubject, mailTo, mailCc, "ServiceReq");
        }

        public static void SendEmailForProblem(DataTable dtActive, DataTable dtAgeingAccepted, DataTable dtWaiting_One, DataTable dtWaiting_Two, string mailSubject, string mailTo, string mailCc)
        {
            SendEmail(dtActive, dtAgeingAccepted, dtWaiting_One, dtWaiting_Two, mailSubject, mailTo, mailCc, "Problem");
        }

        public static void SendEmailForFutureBreach(string mailSubject, string mailTo, string mailCc, DataTable dt)
        {
            SendEmail(mailSubject, mailTo, mailCc, "FutureBreach", dt);
        }

        public static void SendEmailForBreached(string mailSubject, string mailTo, string mailCc, DataTable dt)
        {
            SendEmail(mailSubject, mailTo, mailCc, "Breached", dt);
        }

        private static void SendEmail(string mailSubject, string mailTo, string mailCC, string mailType, DataTable dt)
        {
            try
            {
                string[] allToAddresses = mailTo.Split(";,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                string[] allCcAddresses = mailCC.Split(";,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                oMsg.HTMLBody = GetEmailBodyForBreach(mailType, dt);
                oMsg.Subject = mailSubject;
                Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;
                Outlook.Recipient oRecip;
                foreach (string to in allToAddresses)
                {
                    oRecip = (Outlook.Recipient)oRecips.Add(to);
                    oRecip.Type = (int)Outlook.OlMailRecipientType.olTo;
                    oRecip.Resolve();
                }
                foreach (string cc in allCcAddresses)
                {
                    oRecip = (Outlook.Recipient)oRecips.Add(cc);
                    oRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                    oRecip.Resolve();
                }
                oMsg.Send();

                oRecip = null;
                oRecips = null;
                oMsg = null;
                oApp = null;
            }
            catch (Exception ex)
            {
                Log.write("ERROR: ", ex, ticketStatusLogPath);
            }
        }

        private static string GetEmailBodyForBreach(string mailType, DataTable dt)
        {
            if (mailType == "FutureBreach")
                return FutureBreachEmailBody(dt);
            if (mailType == "Breached")
                return BreachedEmailBody(dt);

            return "";
        }
        private static string FutureBreachEmailBody(DataTable dt)
        {
            Log.write("Generating  Future Breach Email Body", null, ticketStatusLogPath);
            string emailBody = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                string emailText = "Hello,<Br /><Br />Below are the detail of tickets - Solution wise which are going to be breached in the next couple of days.<BR /><Br />" +
                                    "Request you to please take a look at it and take care of the same.<BR /><BR />{0}<BR /><BR />";
                string htmlStart = "<html><head><style id='Book1.3_8882_Styles'><!--table {mso-displayed-decimal-separator:'\\.'; mso-displayed-thousand-separator:'\\,';}.xl158882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl658882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border:1.0pt solid windowtext;background:#DDEBF7;mso-pattern:black none;white-space:nowrap;}.xl668882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#DDEBF7;mso-pattern:black none;white-space:nowrap;}.xl678882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl688882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl698882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl708882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:'General Date';text-align:right;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl718882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:'General Date';text-align:right;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl728882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:right;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl738882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:#0563C1;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:underline;text-underline-style:single;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1.3_8882' align='left'><table border=0 cellpadding=0 cellspacing=0 width=1250 style='border-collapse:collapse;table-layout:fixed;width:939pt'><col width=182 style='mso-width-source:userset;mso-width-alt:6656;width:137pt'><col width=101 style='mso-width-source:userset;mso-width-alt:3693;width:76pt'><col width=659 style='mso-width-source:userset;mso-width-alt:24100;width:494pt'><col width=117 span=2 style='mso-width-source:userset;mso-width-alt:4278;width:88pt'><col width=74 style='mso-width-source:userset;mso-width-alt:2706;width:56pt'><tr height=21 style='height:15.75pt'><td height=21 class=xl658882 width=182 style='height:15.75pt;width:137pt'>Service-Solution</td><td class=xl668882 width=101 style='width:76pt'>BreachDateTime (Task)</td><td class=xl668882 width=659 style='width:494pt'>Subject</td><td class=xl668882 width=117 style='width:88pt'>Parent Object Status</td><td class=xl668882 width=74 style='width:56pt'>Parent Object ID</td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl678882 style='height:15.75pt'>{0}</td><td class=xl688882>{1}</td><td class=xl698882>{2}</td><td class=xl708882>{3}</td><td class=xl718882>{4}</td></tr>";
                string htmlEnd = "</table></div></body></html>";
                StringBuilder sbBody = new StringBuilder();
                foreach (DataRow dr in dt.Rows)
                {
                    sbBody.AppendFormat(body,
                                        dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                        dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                        dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                        dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                        dr[4].ToString() == "0" ? "" : dr[4].ToString()
                                        );


                }
                body = sbBody.ToString();

                emailBody = emailBody + htmlStart + fixedBody + body + htmlEnd;
                emailBody = string.Format(emailText, emailBody) + "<BR /><BR/>**This is auto generated mail, please do not reply to this mail.";
            }
            else
            {
                emailBody = "Hello,<Br /><Br />There are no future breach tickets.<BR /><BR/> **This is auto generated mail, please do not reply to this mail.";
            }
            return emailBody;
        }

        private static string BreachedEmailBody(DataTable dt)
        {
            Log.write("Generating  Breached Email Body", null, ticketStatusLogPath);
            int currentMonth = DateTime.Now.Month;
            string emailBody = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                string emailText = "Hello,<Br /><Br />Below are the detail of tickets - Solution wise which are breached for the month of " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(currentMonth) + " .<BR /><Br />" +
                 "Request you to provide the reason for the breach.<BR /><BR />{0}<BR /><BR />";
                string htmlStart = "<html><head><style id='Book1.3_8882_Styles'><!--table {mso-displayed-decimal-separator:'\\.'; mso-displayed-thousand-separator:'\\,';}.xl158882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl658882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border:1.0pt solid windowtext;background:#DDEBF7;mso-pattern:black none;white-space:nowrap;}.xl668882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#DDEBF7;mso-pattern:black none;white-space:nowrap;}.xl678882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl688882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl698882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl708882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:'General Date';text-align:right;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl718882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:'General Date';text-align:right;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl728882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:right;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl738882{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:#0563C1;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:underline;text-underline-style:single;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1.3_8882' align='left'><table border=0 cellpadding=0 cellspacing=0 width=1250 style='border-collapse:collapse;table-layout:fixed;width:939pt'><col width=182 style='mso-width-source:userset;mso-width-alt:6656;width:137pt'><col width=101 style='mso-width-source:userset;mso-width-alt:3693;width:76pt'><col width=659 style='mso-width-source:userset;mso-width-alt:24100;width:494pt'><col width=117 span=2 style='mso-width-source:userset;mso-width-alt:4278;width:88pt'><col width=74 style='mso-width-source:userset;mso-width-alt:2706;width:56pt'><tr height=21 style='height:15.75pt'><td height=21 class=xl658882 width=182 style='height:15.75pt;width:137pt'>Service-Solution</td><td class=xl668882 width=101 style='width:76pt'>Created On</td><td class=xl668882 width=101 style='width:76pt'>Parent Resolution Target Date</td><td class=xl668882 width=659 style='width:494pt'>Subject</td><td class=xl668882 width=117 style='width:88pt'>BreachPassed (Task)</td><td class=xl668882 width=74 style='width:56pt'>Parent Object ID</td><td class=xl668882 width=74 style='width:56pt'>Resolved On</td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl678882 style='height:15.75pt'>{0}</td><td class=xl688882>{1}</td><td class=xl698882>{2}</td><td class=xl708882>{3}</td><td class=xl718882>{4}</td><td class=xl718882>{5}</td><td class=xl718882>{6}</td></tr>";
                string htmlEnd = "</table></div></body></html>";
                StringBuilder sbBody = new StringBuilder();
                foreach (DataRow dr in dt.Rows)
                {

                    sbBody.AppendFormat(body,
                                        dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                        dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                        dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                        dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                        dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                        dr[5].ToString() == "0" ? "" : dr[5].ToString(),
                                        dr[6].ToString() == "0" ? "" : dr[6].ToString()
                                        );


                }
                body = sbBody.ToString();

                emailBody = emailBody + htmlStart + fixedBody + body + htmlEnd;
                emailBody = string.Format(emailText, emailBody) + "Breach reason needs to be updated in below link.<BR /><BR />" + sharePointUrl + "<BR /><BR/>**This is auto generated mail, please do not reply to this mail.";
            }
            else
            {
                emailBody = "Hello,<Br /><Br />There are no breached tickets.<BR /><BR/> **This is auto generated mail, please do not reply to this mail.";
            }
            return emailBody;
        }

        private static void SendEmail(DataTable dtActive, DataTable dtAgeingAccepted, DataTable dtWaiting_One, DataTable dtWaiting_Two, string mailSubject, string mailTo, string mailCc, string mailType)
        {
            try
            {
                string[] allToAddresses = mailTo.Split(";,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                string[] allCcAddresses = mailCc.Split(";,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                oMsg.HTMLBody = GetEmailBody(dtActive, dtAgeingAccepted, dtWaiting_One, dtWaiting_Two, mailType);
                oMsg.Subject = mailSubject;
                Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;
                Outlook.Recipient oRecip;
                foreach (string to in allToAddresses)
                {
                    oRecip = (Outlook.Recipient)oRecips.Add(to);
                    oRecip.Type = (int)Outlook.OlMailRecipientType.olTo;
                    oRecip.Resolve();
                }

                foreach (string cc in allCcAddresses)
                {
                    oRecip = (Outlook.Recipient)oRecips.Add(cc);
                    oRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                    oRecip.Resolve();
                }

                oMsg.Send();
                oRecip = null;
                oRecips = null;
                oMsg = null;
                oApp = null;
            }
            catch (Exception ex)
            {
                Log.write("ERROR: ", ex, ticketStatusLogPath);
            }
        }

        private static string GetEmailBody(DataTable dtActive, DataTable dtAgeingAccepted, DataTable dtWaiting_One, DataTable dtWaiting_Two, string mailType)
        {

            if (mailType == "Incident")
                return IncidentEmailBody(dtActive, dtAgeingAccepted, dtWaiting_One, dtWaiting_Two);

            if (mailType == "ServiceReq")
                return ServiceRequestEmailBody(dtActive, dtAgeingAccepted, dtWaiting_One, dtWaiting_Two);

            if (mailType == "Problem")
                return ProblemEmailBody(dtActive, dtAgeingAccepted);

            return "";
        }

        private static string IncidentEmailBody(DataTable dtActive, DataTable dtAgeingAccepted, DataTable dtWaiting_One, DataTable dtWaiting_Two)
        {
            Log.write("Genrating  Incident Email Body", null, ticketStatusLogPath);
            string emailBody = "";
            string emailText = "Hi Team, <Br /><Br />Please find the below details of Total Active tickets - Solution wise. <BR /><Br />{0}<BR /><BR />";

            if (dtActive.Rows.Count > 1)
            {
                string htmlStart = "<html><head><style id='Book1_5117_Styles'><!--table {mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl155117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl635117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl645117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl655117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid #9BC2E6;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl665117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl675117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl685117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:none;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl695117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl705117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl715117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl725117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl735117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_5117' align=left' ><table border = 0 cellpadding = 0 cellspacing = 0 width = 709 style = 'border-collapse:collapse;table-layout:fixed;width:532pt' ><col width = 155 style = 'mso-width-source:userset;mso-width-alt:5668;width:116pt' ><col width = 96 style = 'mso-width-source:userset;mso-width-alt:3510;width:72pt' ><col width = 110 style = 'mso-width-source:userset;mso-width-alt:4022;width:83pt' ><col width = 98 style = 'mso-width-source:userset;mso-width-alt:3584;width:74pt' ><col width = 88 style = 'mso-width-source:userset;mso-width-alt:3218;width:66pt' ><col width = 83 style = 'mso-width-source:userset;mso-width-alt:3035;width:62pt' ><col width = 79 style = 'mso-width-source:userset;mso-width-alt:2889;width:59pt' ><tr height = 26 style = 'height:19.5pt;mso-yfti-firstrow:yes;mso-yfti-irow:0' ><td colspan = 7 height = 26 class=xl715117 width = 709 style='border-right:1.0pt solid black;height:19.5pt;width:532pt'>Overall Incident Status</td></tr><tr height = 21 style='height:15.75pt;mso-yfti-irow:1'><td height = 21 class=xl635117 style = 'height:15.75pt' > Service-Solution </td ><td class=xl645117>Assigned</td><td class=xl655117>Reassigned</td><td class=xl645117>Accepted</td><td class=xl645117>Waiting<td class=xl645117>Logged</td><td class=xl645117>Grand Total</td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl665117 style='height:15.75pt'>{0}</td><td class=xl675117>{1}</td><td class=xl675117>{2}</td><td class=xl675117>{3}</td><td class=xl675117>{4}</td><td class=xl675117>{5}</td><td class=xl675117>{6}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:14;mso-yfti-lastrow:yes'><td height=21 class=xl705117 style='height:15.75pt'>{0}</td><td class=xl645117>{1}</td><td class=xl645117>{2}</td><td class=xl645117>{3}</td><td class=xl645117>{4}</td><td class=xl645117>{5}</td><td class=xl645117>{6}</td></tr>";
                string htmlEnd = "</table></div></body></html>";
                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtActive.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtActive.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString(),
                                            dr[6].ToString() == "0" ? "" : dr[6].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString(),
                                            dr[6].ToString() == "0" ? "" : dr[6].ToString());
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            if (dtAgeingAccepted.Rows.Count > 1)
            {
                DataTable dtTemp = dtAgeingAccepted.DefaultView.ToTable(false, "Solution", "< 7 days", "Grand Total");
                string htmlStart = "<html><head><style id='Book1_12853_Styles'><!--table{mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl1512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri,sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;so-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6612853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6712853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:'Times New Roman', serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6812853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6912853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7012853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7112853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7212853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#4472C4;mso-pattern:black none;white-space:nowrap;}.xl7312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl7512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_12853' align=left><table border=0 cellpadding=0 cellspacing=0 width=409 style='border-collapse: collapse;table-layout:fixed;width:308pt'><col width=222 style='mso-width-source:userset;mso-width-alt:8118;width:167pt'><col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'><col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'><tr height=26 style='height:19.5pt'><td colspan=2 height=26 class=xl6512853 width=331 style='border-right:1.0pt solid black;height:19.5pt;width:249pt'>Ageing report of Accepted Incidents</td><td class=xl6712853 width=78 style='width:59pt;padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl6812853 style='height:15.75pt'>Parent Object</td><td class=xl6912853>Incident</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl7012853 style='height:15.75pt'>Status</td><td class=xl7112853>Accepted</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:3'><td height=21 class=xl7012853 style='height:15.75pt'>Parent Object Status</td><td class=xl7112853>(Multiple Items)</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:4'><td height=21 class=xl7212853 style='height:15.75pt'>#Tickets -Waiting status-Age wise</td><td class=xl7312853>&nbsp;</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>Count of Parent Object ID</td><td class=xl6412853>Column Labels</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:6'><td height=21 class=xl7412853 style='height:15.75pt'>Service-Solution</td><td class=xl7512853>&lt; 7 days</td><td class=xl7512853>Grand Total</td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:7'><td height=21 class=xl6312853 style='height:15.75pt'>{0}</td><td class=xl6412853>{1}</td><td class=xl6412853>{2}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:11;mso-yfti-lastrow:yes'><td height=21 class=xl7412853 style='height:15.75pt'>{0}</td><td class=xl7412853>{1}</td><td class=xl7412853>{2}</td></tr>";
                string htmlEnd = "</table></div></body></html>";

                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();

                int counter = 0;
                foreach (DataRow dr in dtTemp.Rows)
                {
                    counter++;
                    int rowCount = dtTemp.Rows.Count;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString());
                }



                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + "<br /><br />" + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            if (dtWaiting_One.Rows.Count > 1)
            {
                string htmlStart = "<html><head><style id='Book1_12853_Styles'><!--table{mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl1512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri,sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;so-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6612853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6712853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:'Times New Roman', serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6812853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6912853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7012853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7112853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7212853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#4472C4;mso-pattern:black none;white-space:nowrap;}.xl7312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl7512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_12853' align=left><table border=0 cellpadding=0 cellspacing=0 width=409 style='border-collapse: collapse;table-layout:fixed;width:308pt'><col width=222 style='mso-width-source:userset;mso-width-alt:8118;width:167pt'><col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'><col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'><tr height=26 style='height:19.5pt'><td colspan=2 height=26 class=xl6512853 width=331 style='border-right:1.0pt solid black;height:19.5pt;width:249pt'>Ageing report of Waiting Incidents</td><td class=xl6712853 width=78 style='width:59pt;padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl6812853 style='height:15.75pt'>Parent Object</td><td class=xl6912853>Incident</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl7012853 style='height:15.75pt'>Status</td><td class=xl7112853>Waiting</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:3'><td height=21 class=xl7012853 style='height:15.75pt'>Parent Object Status</td><td class=xl7112853>(Multiple Items)</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:4'><td height=21 class=xl7212853 style='height:15.75pt'>#Tickets -Waiting status-Age wise</td><td class=xl7312853>&nbsp;</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>Count of Parent ID</td><td class=xl6412853>Column Labels</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:6'><td height=21 class=xl7412853 style='height:15.75pt'>Service-Solution</td><td class=xl7512853> &lt; 7 days </td><td class=xl7512853> 7 days to 14 days </td><td class=xl7512853> 14 days to 30 days </td><td class=xl7512853> &gt;= 30 days </td><td class=xl7512853> Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:7'><td height=21 class=xl6312853 style='height:15.75pt'>{0}</td><td class=xl6412853>{1}</td><td class=xl6412853>{2}</td><td class=xl6412853>{3}</td><td class=xl6412853>{4}</td><td class=xl6412853>{5}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:11;mso-yfti-lastrow:yes'><td height=21 class=xl7412853 style='height:15.75pt'>{0}</td><td class=xl7412853>{1}</td><td class=xl7412853>{2}</td><td class=xl7412853>{3}</td><td class=xl7412853>{4}</td><td class=xl7412853>{5}</td></tr>";
                string htmlEnd = "</table></div></body></html>";

                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtWaiting_One.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtWaiting_One.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString());
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + "<br /><br />" + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            if (dtWaiting_Two.Rows.Count > 1)
            {
                string htmlStart = "<html><head><style id='Book1_12853_Styles'><!--table{mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl1512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri,sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;so-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6612853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6712853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:'Times New Roman', serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6812853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6912853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7012853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7112853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7212853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#4472C4;mso-pattern:black none;white-space:nowrap;}.xl7312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl7512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_12853' align=left><table border=0 cellpadding=0 cellspacing=0 width=409 style='border-collapse: collapse;table-layout:fixed;width:308pt'><col width=222 style='mso-width-source:userset;mso-width-alt:8118;width:167pt'><col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'><col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'><tr height=26 style='height:19.5pt'><td colspan=2 height=26 class=xl6512853 width=331 style='border-right:1.0pt solid black;height:19.5pt;width:249pt'>Ageing report of Waiting Incidents</td><td class=xl6712853 width=78 style='width:59pt;padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl6812853 style='height:15.75pt'>Parent Object</td><td class=xl6912853>Incident</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>&nbsp;</td><td class=xl6412853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>Count of Parent ID</td><td class=xl6412853>Column Labels</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:6'><td height=21 class=xl7412853 style='height:15.75pt'>Service-Solution</td><td class=xl7512853>Waiting for Customer </td><td class=xl7512853>Waiting for 3rd Party </td><td class=xl7512853>Waiting for Change</td><td class=xl7512853> Waiting for Confirmation </td><td class=xl7512853> Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:7'><td height=21 class=xl6312853 style='height:15.75pt'>{0}</td><td class=xl6412853>{1}</td><td class=xl6412853>{2}</td><td class=xl6412853>{3}</td><td class=xl6412853>{4}</td><td class=xl6412853>{5}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:11;mso-yfti-lastrow:yes'><td height=21 class=xl7412853 style='height:15.75pt'>{0}</td><td class=xl7412853>{1}</td><td class=xl7412853>{2}</td><td class=xl7412853>{3}</td><td class=xl6412853>{4}</td><td class=xl7412853>{5}</tr>";
                string htmlEnd = "</table></div></body></html>";

                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtWaiting_Two.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtWaiting_Two.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                                dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                                dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                                dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                                dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                                dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                                dr[5].ToString() == "0" ? "" : dr[5].ToString());
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + "<br /><br />" + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }
            emailBody = string.Format(emailText, emailBody) + "Please refer to below link for Daily Ageing report & SLA Resolutions in T - Room.<BR /><BR />" + sharePointUrl + "<BR /><BR/>**This is auto generated mail, please do not reply to this mail.";
            return emailBody;
        }


        private static string ProblemEmailBody(DataTable dtActive, DataTable dtAgeingAccepted)
        {
            Log.write("Genrating  Problem Email Body", null, ticketStatusLogPath);

            string emailBody = "";
            string emailText = "Hi Team, <Br /><Br />Please find the below details of Total Active tickets - Solution wise. <BR /><Br />{0}<BR /><BR />";
            if (dtActive.Rows.Count > 1)
            {
                DataTable dtTemp = dtActive.DefaultView.ToTable(false, "Service-Solution", "Assigned", "Accepted","Waiting", "Grand Total");

                string htmlStart = "<html><head><style id='Book1_5117_Styles'><!--table {mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl155117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl635117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl645117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl655117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid #9BC2E6;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl665117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl675117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl685117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:none;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl695117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl705117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl715117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl725117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl735117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_5117' align=left' ><table border = 0 cellpadding = 0 cellspacing = 0 width = 709 style = 'border-collapse:collapse;table-layout:fixed;width:532pt' ><col width = 155 style = 'mso-width-source:userset;mso-width-alt:5668;width:116pt' ><col width = 96 style = 'mso-width-source:userset;mso-width-alt:3510;width:72pt' ><col width = 110 style = 'mso-width-source:userset;mso-width-alt:4022;width:83pt' ><col width = 98 style = 'mso-width-source:userset;mso-width-alt:3584;width:74pt' ><col width = 88 style = 'mso-width-source:userset;mso-width-alt:3218;width:66pt' ><col width = 83 style = 'mso-width-source:userset;mso-width-alt:3035;width:62pt' ><col width = 79 style = 'mso-width-source:userset;mso-width-alt:2889;width:59pt' ><tr height = 26 style = 'height:19.5pt;mso-yfti-firstrow:yes;mso-yfti-irow:0' ><td colspan = 5 height = 26 class=xl715117 width = 709 style='border-right:1.0pt solid black;height:19.5pt;width:532pt'>OverAll Problem Ticket Status</td></tr><tr height = 21 style='height:15.75pt;mso-yfti-irow:1'><td height = 21 class=xl635117 style = 'height:15.75pt' > Service-Solution </td ><td class=xl645117>Assigned </td><td class=xl645117>Accepted </td> <td class=xl645117>Waiting </td> <td class=xl645117> Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl665117 style='height:15.75pt'>{0}</td><td class=xl675117>{1}</td><td class=xl675117>{2}</td><td class=xl675117>{3}</td><td class=xl675117>{4}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:14;mso-yfti-lastrow:yes'><td height=21 class=xl705117 style='height:15.75pt'>{0}</td><td class=xl645117>{1}</td><td class=xl645117>{2}</td><td class=xl645117>{3}</td><td class=xl675117>{4}</td></tr>";
                string htmlEnd = "</table></div></body></html>";
                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtTemp.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtTemp.Rows)
                {
                    counter++;
                    int rowTotal = 0;
                    if (counter <= rowCount)
                    {
                        rowTotal = Convert.ToInt32(dr[1]) + Convert.ToInt32(dr[2])+ Convert.ToInt32(dr[3]);
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            rowTotal == 0 ? "" : rowTotal.ToString());
                    }
                    else
                    {
                        rowTotal = Convert.ToInt32(dr[1]) + Convert.ToInt32(dr[2])+ Convert.ToInt32(dr[3]);
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            rowTotal == 0 ? "" : rowTotal.ToString());
                    }
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            if (dtAgeingAccepted.Rows.Count > 1)
            {
                DataTable dtTemp = dtAgeingAccepted.DefaultView.ToTable(false, "Solution", ">= 30 days", "Grand Total");
                string htmlStart = "<html><head><style id='Book1_12853_Styles'><!--table{mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl1512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri,sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;so-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6612853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6712853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:'Times New Roman', serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6812853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6912853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7012853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7112853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7212853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#4472C4;mso-pattern:black none;white-space:nowrap;}.xl7312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl7512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_12853' align=left><table border=0 cellpadding=0 cellspacing=0 width=409 style='border-collapse: collapse;table-layout:fixed;width:308pt'><col width=222 style='mso-width-source:userset;mso-width-alt:8118;width:167pt'><col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'><col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'><tr height=26 style='height:19.5pt'><td colspan=2 height=26 class=xl6512853 width=331 style='border-right:1.0pt solid black;height:19.5pt;width:249pt'>Ageing report of Accepted Problem Ticket</td><td class=xl6712853 width=78 style='width:59pt;padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl6812853 style='height:15.75pt'>Parent Object</td><td class=xl6912853>Problem</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl7012853 style='height:15.75pt'>Status</td><td class=xl7112853>Accepted</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:3'><td height=21 class=xl7012853 style='height:15.75pt'>Parent Object Status</td><td class=xl7112853>(Multiple Items)</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:4'><td height=21 class=xl7212853 style='height:15.75pt'>#Tickets -Waiting status-Age wise</td><td class=xl7312853>&nbsp;</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>Count of Parent Object ID</td><td class=xl6412853>Column Labels</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:6'><td height=21 class=xl7412853 style='height:15.75pt'>Service-Solution</td><td class=xl7512853> &gt;= 30 days </td><td class=xl7512853> Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:7'><td height=21 class=xl6312853 style='height:15.75pt'>{0}</td><td class=xl6412853>{1}</td><td class=xl6412853>{2}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:11;mso-yfti-lastrow:yes'><td height=21 class=xl7412853 style='height:15.75pt'>{0}</td><td class=xl7412853>{1}</td><td class=xl7412853>{2}</td></tr>";
                string htmlEnd = "</table></div></body></html>";

                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtTemp.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtTemp.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString());
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + "<br /><br />" + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }
            emailBody = string.Format(emailText, emailBody) + "Please refer to below link for Daily Ageing report & SLA Resolutions in T - Room.<BR /><BR />" + sharePointUrl + "<BR /><BR/>**This is auto generated mail, please do not reply to this mail.";
            return emailBody;
        }

        private static string ServiceRequestEmailBody(DataTable dtActive, DataTable dtAgeingAccepted, DataTable dtWaiting_One, DataTable dtWaiting_Two)
        {
            Log.write("Genrating  ServiceRequest Email Body", null, ticketStatusLogPath);
            string emailBody = "";
            string emailText = "Hi Team, <Br /><Br />Please find the below details of Total Active tickets - Solution wise. <BR /><Br />{0}<BR /><BR />";
            if (dtActive.Rows.Count > 1)
            {
                string htmlStart = "<html><head><style id='Book1_5117_Styles'><!--table {mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl155117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl635117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl645117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl655117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid #9BC2E6;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl665117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl675117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl685117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:none;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl695117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl705117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl715117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl725117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl735117{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_5117' align=left' ><table border = 0 cellpadding = 0 cellspacing = 0 width = 709 style = 'border-collapse:collapse;table-layout:fixed;width:532pt' ><col width = 155 style = 'mso-width-source:userset;mso-width-alt:5668;width:116pt' ><col width = 96 style = 'mso-width-source:userset;mso-width-alt:3510;width:72pt' ><col width = 110 style = 'mso-width-source:userset;mso-width-alt:4022;width:83pt' ><col width = 98 style = 'mso-width-source:userset;mso-width-alt:3584;width:74pt' ><col width = 88 style = 'mso-width-source:userset;mso-width-alt:3218;width:66pt' ><col width = 83 style = 'mso-width-source:userset;mso-width-alt:3035;width:62pt' ><col width = 79 style = 'mso-width-source:userset;mso-width-alt:2889;width:59pt' ><tr height = 26 style = 'height:19.5pt;mso-yfti-firstrow:yes;mso-yfti-irow:0' ><td colspan = 7 height = 26 class=xl715117 width = 709 style='border-right:1.0pt solid black;height:19.5pt;width:532pt'>OverAll Service Request Status</td></tr><tr height = 21 style='height:15.75pt;mso-yfti-irow:1'><td height = 21 class=xl635117 style = 'height:15.75pt' > Service-Solution </td ><td class=xl645117> Assigned </td><td class=xl655117> Reassigned </td><td class=xl645117>Accepted </td><td class=xl645117>Waiting </td><td class=xl645117> Logged </td><td class=xl645117 > Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl665117 style='height:15.75pt'>{0}</td><td class=xl675117>{1}</td><td class=xl675117>{2}</td><td class=xl675117>{3}</td><td class=xl675117>{4}</td><td class=xl675117>{5}</td><td class=xl675117>{6}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:14;mso-yfti-lastrow:yes'><td height=21 class=xl705117 style='height:15.75pt'>{0}</td><td class=xl645117>{1}</td><td class=xl645117>{2}</td><td class=xl645117>{3}</td><td class=xl645117>{4}</td><td class=xl675117>{5}</td><td class=xl645117>{6}</td></tr>";
                string htmlEnd = "</table></div></body></html>";
                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtActive.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtActive.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString(),
                                            dr[6].ToString() == "0" ? "" : dr[6].ToString(),
                                            dr[7].ToString() == "0" ? "" : dr[7].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString(),
                                            dr[6].ToString() == "0" ? "" : dr[6].ToString(),
                                            dr[7].ToString() == "0" ? "" : dr[7].ToString());
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            if (dtAgeingAccepted.Rows.Count > 1)
            {
                string htmlStart = "<html><head><style id='Book1_12853_Styles'><!--table{mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl1512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri,sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;so-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6612853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6712853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:'Times New Roman', serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6812853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6912853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7012853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7112853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7212853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#4472C4;mso-pattern:black none;white-space:nowrap;}.xl7312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl7512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_12853' align=left><table border=0 cellpadding=0 cellspacing=0 width=409 style='border-collapse: collapse;table-layout:fixed;width:308pt'><col width=222 style='mso-width-source:userset;mso-width-alt:8118;width:167pt'><col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'><col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'><tr height=26 style='height:19.5pt'><td colspan=2 height=26 class=xl6512853 width=331 style='border-right:1.0pt solid black;height:19.5pt;width:249pt'>Ageing report of Accepted Service Request</td><td class=xl6712853 width=78 style='width:59pt;padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl6812853 style='height:15.75pt'>Parent Object</td><td class=xl6912853>Service Request</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl7012853 style='height:15.75pt'>Status</td><td class=xl7112853>Accepted</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:3'><td height=21 class=xl7012853 style='height:15.75pt'>Parent Object Status</td><td class=xl7112853>(Multiple Items)</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:4'><td height=21 class=xl7212853 style='height:15.75pt'>#Tickets -Waiting status-Age wise</td><td class=xl7312853>&nbsp;</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>Count of Parent Object ID</td><td class=xl6412853>Column Labels</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:6'><td height=21 class=xl7412853 style='height:15.75pt'> Service-Solution </td><td class=xl7512853> &lt; 7 days </td><td class=xl7512853> 7 days to 14 days </td><td class=xl7512853> 14 days to 30 days </td><td class=xl7512853> &gt;= 30 days </td><td class=xl7512853> Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:7'><td height=21 class=xl6312853 style='height:15.75pt'>{0}</td><td class=xl6412853>{1}</td><td class=xl6412853>{2}</td><td class=xl6412853>{3}</td><td class=xl6412853>{4}</td><td class=xl6412853>{5}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:11;mso-yfti-lastrow:yes'><td height=21 class=xl7412853 style='height:15.75pt'>{0}</td><td class=xl7412853>{1}</td><td class=xl7412853>{2}</td><td class=xl7412853>{3}</td><td class=xl7412853>{4}</td><td class=xl7412853>{5}</td></tr>";
                string htmlEnd = "</table></div></body></html>";

                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtAgeingAccepted.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtAgeingAccepted.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString());
                }



                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + "<br /><br />" + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            if (dtWaiting_One.Rows.Count > 1)
            {
                string htmlStart = "<html><head><style id='Book1_12853_Styles'><!--table{mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl1512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri,sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;so-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6612853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6712853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:'Times New Roman', serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6812853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6912853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7012853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7112853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7212853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#4472C4;mso-pattern:black none;white-space:nowrap;}.xl7312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl7512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_12853' align=left><table border=0 cellpadding=0 cellspacing=0 width=409 style='border-collapse: collapse;table-layout:fixed;width:308pt'><col width=222 style='mso-width-source:userset;mso-width-alt:8118;width:167pt'><col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'><col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'><tr height=26 style='height:19.5pt'><td colspan=2 height=26 class=xl6512853 width=331 style='border-right:1.0pt solid black;height:19.5pt;width:249pt'>Ageing report of Waiting Service Request</td><td class=xl6712853 width=78 style='width:59pt;padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl6812853 style='height:15.75pt'>Parent Object</td><td class=xl6912853>Service Request</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl7012853 style='height:15.75pt'>Status</td><td class=xl7112853>Waiting</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:3'><td height=21 class=xl7012853 style='height:15.75pt'>Parent Object Status</td><td class=xl7112853>(Multiple Items)</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:4'><td height=21 class=xl7212853 style='height:15.75pt'>#Tickets -Waiting status-Age wise</td><td class=xl7312853>&nbsp;</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>Count of Parent Object ID</td><td class=xl6412853>Column Labels</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:6'><td height=21 class=xl7412853 style='height:15.75pt'>Service-Solution</td><td class=xl7512853> &lt; 7 days </td><td class=xl7512853> 7 days to 14 days </td><td class=xl7512853> 14 days to 30 days </td><td class=xl7512853> &gt;= 30 days </td><td class=xl7512853> Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:7'><td height=21 class=xl6312853 style='height:15.75pt'>{0}</td><td class=xl6412853>{1}</td><td class=xl6412853>{2}</td><td class=xl6412853>{3}</td><td class=xl6412853>{4}</td><td class=xl6412853>{5}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:11;mso-yfti-lastrow:yes'><td height=21 class=xl7412853 style='height:15.75pt'>{0}</td><td class=xl7412853>{1}</td><td class=xl7412853>{2}</td><td class=xl7412853>{3}</td><td class=xl7412853>{4}</td><td class=xl7412853>{5}</td></tr>";
                string htmlEnd = "</table></div></body></html>";

                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtWaiting_One.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtWaiting_One.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString());
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + "<br /><br />" + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            if (dtWaiting_Two.Rows.Count > 1)
            {
                string htmlStart = "<html><head><style id='Book1_12853_Styles'><!--table{mso-displayed-decimal-separator:'\\.';mso-displayed-thousand-separator:'\\,';}.xl1512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri,sans-serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;so-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:none;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6612853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:14.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid black;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl6712853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:10.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:'Times New Roman', serif;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6812853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl6912853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7012853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:1.0pt solid windowtext;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7112853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:none;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7212853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:1.0pt solid windowtext;background:#4472C4;mso-pattern:black none;white-space:nowrap;}.xl7312853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:1.0pt solid windowtext;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl7412853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:1.0pt solid windowtext;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}.xl7512853{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border-top:none;border-right:1.0pt solid windowtext;border-bottom:1.0pt solid windowtext;border-left:none;background:#9BC2E6;mso-pattern:black none;white-space:nowrap;}--></style></head>";
                string fixedBody = "<body><div id='Book1_12853' align=left><table border=0 cellpadding=0 cellspacing=0 width=409 style='border-collapse: collapse;table-layout:fixed;width:308pt'><col width=222 style='mso-width-source:userset;mso-width-alt:8118;width:167pt'><col width=109 style='mso-width-source:userset;mso-width-alt:3986;width:82pt'><col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'><tr height=26 style='height:19.5pt'><td colspan=2 height=26 class=xl6512853 width=331 style='border-right:1.0pt solid black;height:19.5pt;width:249pt'>Ageing report of Waiting Service Request</td><td class=xl6712853 width=78 style='width:59pt;padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:1'><td height=21 class=xl6812853 style='height:15.75pt'>Parent Object</td><td class=xl6912853>Service Request</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:2'><td height=21 class=xl7012853 style='height:15.75pt'>Status</td><td class=xl7112853>Waiting</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:4'><td height=21 class=xl7212853 style='height:15.75pt'>#Tickets -Waiting status-Status wise</td><td class=xl7312853>&nbsp;</td><td class=xl6712853 style='padding-bottom:0in;padding-top:0in'></td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:5'><td height=21 class=xl6312853 style='height:15.75pt'>Count of Parent Object ID</td><td class=xl6412853>Column Labels</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td><td class=xl7312853>&nbsp;</td></tr><tr height=21 style='height:15.75pt;mso-yfti-irow:6'><td height=21 class=xl7412853 style='height:15.75pt'>Service-Solution</td><td class=xl7512853>Waiting for Customer </td><td class=xl7512853>Waiting for 3rd Party </td><td class=xl7512853>Waiting for Change</td><td class=xl7512853>In Progress</td><td class=xl7512853>Waiting for Confirmation </td><td class=xl7512853> Grand Total </td></tr>";
                string body = "<tr height=21 style='height:15.75pt;mso-yfti-irow:7'><td height=21 class=xl6312853 style='height:15.75pt'>{0}</td><td class=xl6412853>{1}</td><td class=xl6412853>{2}</td><td class=xl6412853>{3}</td><td class=xl6412853>{4}</td><td class=xl6412853>{5}</td><td class=xl6412853>{6}</td></tr>";
                string lastRow = "<tr height=21 style='height:15.75pt;mso-yfti-irow:11;mso-yfti-lastrow:yes'><td height=21 class=xl7412853 style='height:15.75pt'>{0}</td><td class=xl7412853>{1}</td><td class=xl7412853>{2}</td><td class=xl7412853>{3}</td><td class=xl7412853>{4}</td><td class=xl7412853>{5}</td><td class=xl6412853>{6}</td></tr>";
                string htmlEnd = "</table></div></body></html>";

                StringBuilder sbBody = new StringBuilder();
                StringBuilder sbLastRow = new StringBuilder();
                int rowCount = dtWaiting_Two.Rows.Count;
                int counter = 0;
                foreach (DataRow dr in dtWaiting_Two.Rows)
                {
                    counter++;
                    if (counter != rowCount)
                        sbBody.AppendFormat(body,
                                            dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                            dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                            dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                            dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                            dr[4].ToString() == "0" ? "" : dr[4].ToString(),
                                            dr[5].ToString() == "0" ? "" : dr[5].ToString(),
                                            dr[6].ToString() == "0" ? "" : dr[6].ToString());
                    else
                        sbLastRow.AppendFormat(lastRow,
                                                dr[0].ToString() == "0" ? "" : dr[0].ToString(),
                                                dr[1].ToString() == "0" ? "" : dr[1].ToString(),
                                                dr[2].ToString() == "0" ? "" : dr[2].ToString(),
                                                dr[3].ToString() == "0" ? "" : dr[3].ToString(),
                                                dr[5].ToString() == "0" ? "" : dr[5].ToString(),
                                                dr[6].ToString() == "0" ? "" : dr[6].ToString());
                }
                body = sbBody.ToString();
                lastRow = sbLastRow.ToString();
                emailBody = emailBody + "<br /><br />" + htmlStart + fixedBody + body + lastRow + htmlEnd;
            }

            emailBody = string.Format(emailText, emailBody) + "Please refer to below link for Daily Ageing report & SLA Resolutions in T - Room.<BR /><BR />" + sharePointUrl + "<BR /><BR/>**This is auto generated mail, please do not reply to this mail.";
            return emailBody;
        }

        //Send the  patching notification email for the respective environment
        public static void SendPatchingEmail(DateTime patchDate, string environment, string activityType, string patchingLogPath)
        {
            if (activityType.Equals(Constants.mspatching))
            {
                Log.write("Executing SendPatchingEmail for env-->" + environment + " at time " + DateTime.Now.ToString(), null, patchingLogPath);
            }
            else if (activityType.Equals(Constants.nugetpatching))
            {
                Log.write("Executing SendPatchingEmail at time " + DateTime.Now.ToString(), null, patchingLogPath);
            }
            else if (activityType.Equals(Constants.CCBMail))
            {
                Log.write("Executing CCBMail at time " + DateTime.Now.ToString(), null, CCBMailLogPath);
            }
            else if (activityType.Equals(Constants.CCBPublishMail))
            {
                Log.write("Executing CCBPublishMail at time " + DateTime.Now.ToString(), null, CCBMailLogPath);
            }

            try
            {
                bool sendMailFlag = false;
                Outlook.Application oApp = new Outlook.Application();
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                if (activityType.Equals(Constants.mspatching))
                {
                    oMsg.HTMLBody = GetMsPatchingEmailBody(patchDate, environment);
                    oMsg.Subject = GetPatchingEmailSubject(patchDate, environment);
                }
                else if (activityType.Equals(Constants.nugetpatching))
                {
                    oMsg.HTMLBody = GetNugetPatchingEmailBody();
                    oMsg.Subject = GetNugetPatchingEmailSubject(patchDate);
                }
                else if (activityType.Equals(Constants.CCBMail))
                {
                    oMsg.HTMLBody = GetCCBEmailBody(patchDate); //.Replace('ImagePlaceholder', inlineLogo);
                    oMsg.Subject = GetCCBEmailSubject(patchDate);

                }
                else if (activityType.Equals(Constants.CCBPublishMail))
                {

                    oMsg.HTMLBody = GetCCBPublishEmailBody(patchDate);
                    oMsg.Subject = GetCCBPublishEmailSubject(patchDate);
                }
                oMsg.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;

                if (!string.IsNullOrEmpty(oMsg.HTMLBody))
                {
                    //Fetch To and CC Distribution list .txt files from configured paths
                    string toRecipientsFilePath = Path.GetFullPath(ConfigurationManager.AppSettings["ToRecipientsListFile"]);
                    string ccRecipientsFilePath = Path.GetFullPath(ConfigurationManager.AppSettings["CCRecipientsListFile"]);

                    //Fetch To and CC Distribution email-ids in list format from the .txt files
                    List<string> emailToList = GetRecipientsList(toRecipientsFilePath);
                    List<string> emailCCList = GetRecipientsList(ccRecipientsFilePath);

                    if (emailToList.Count > 0 && emailCCList.Count > 0)
                    {
                        //If all the configured recipients are added then sendMailFlag is set to True
                        sendMailFlag = AddRecipients(ref oMsg, emailToList, emailCCList);

                        if (sendMailFlag)
                        {
                            oMsg.Send();
                            if (activityType.Equals(Constants.mspatching))
                            {
                                Log.write("MS Patching Email for " + environment + " environment sent at " + DateTime.Now.ToString(), null, patchingLogPath);
                            }
                            else if (activityType.Equals(Constants.nugetpatching))
                            {
                                Log.write("Nuget Patching email sent at " + DateTime.Now.ToString(), null, NugetPatchingLogPath);
                            }
                            else if (activityType.Equals(Constants.CCBMail))
                            {
                                Log.write("CCB Mail sent at " + DateTime.Now.ToString(), null, CCBMailLogPath);
                            }
                            else if (activityType.Equals(Constants.CCBPublishMail))
                            {
                                Log.write("CCB Mail sent at " + DateTime.Now.ToString(), null, CCBMailLogPath);
                            }
                        }
                        else
                        {
                            Log.write("Error while adding recipients to the Outlook email thus no notification email would be sent ", null, patchingLogPath);
                        }
                    }
                    else
                    {
                        Log.write("Either To or CC ditribution list files seems to be empty thus no notification email would be sent.", null, patchingLogPath);
                    }
                }
                else
                {
                    if (activityType.Equals(Constants.mspatching))
                    {
                        Log.write("Couldnt fetch email body for the environment " + environment + " from its template, thus no notification email would be sent.", null, patchingLogPath);
                    }
                    else if (activityType.Equals(Constants.nugetpatching))
                    {
                        Log.write("Couldnt fetch email body from its template, thus no notification email would be sent.", null, patchingLogPath);
                    }
                    else if (activityType.Equals(Constants.CCBMail))
                    {
                        Log.write("Couldn't fectch email body from it's template,thus no notification email would be sent.", null, CCBMailLogPath);
                    }
                }
            }
            catch (ConfigurationErrorsException ex)
            {
                Log.write("ConfigurationErrorsException exception in the SendPatchingEmail method, thus no notification email would be sent", ex, patchingLogPath);
            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception in the SendPatchingEmail method, thus no notification email would be sent", ex, patchingLogPath);
            }
            catch (PathTooLongException ex)
            {
                Log.write("PathTooLongException exception in the SendPatchingEmail method, thus no notification email would be sent", ex, patchingLogPath);
            }
            catch (System.Security.SecurityException ex)
            {
                Log.write("SecurityException exception in the SendPatchingEmail method", ex, patchingLogPath);
            }
        }

        //Adds the TO and CC recipients to the MS Patching email.
        private static bool AddRecipients(ref Outlook.MailItem mail, List<string> mailToList, List<string> mailCCList)
        {
            bool retValue = false;

            Outlook.Recipients recipients = null;
            Outlook.Recipient recipientTo = null;
            Outlook.Recipient recipientCC = null;
            try
            {
                recipients = mail.Recipients;
                while (recipients.Count != 0)
                {
                    recipients.Remove(1);
                }
                for (int count = 0; count < mailToList.Count; count++)
                {
                    recipientTo = recipients.Add(mailToList[count]);
                    recipientTo.Type = (int)Outlook.OlMailRecipientType.olTo;
                    recipientTo.Resolve();
                }
                for (int count = 0; count < mailCCList.Count; count++)
                {
                    recipientCC = recipients.Add(mailCCList[count]);
                    recipientCC.Type = (int)Outlook.OlMailRecipientType.olCC;
                    recipientCC.Resolve();
                }
                retValue = recipients.ResolveAll();
            }
            catch (OverflowException ex)
            {
                Log.write("OverflowException exception in the AddRecipients method", ex, MsPatchingLogPath);
            }
            catch (IndexOutOfRangeException ex)
            {
                Log.write("IndexOutOfRangeException exception in the AddRecipients method", ex, MsPatchingLogPath);
            }
            finally
            {
                if (recipientCC != null) Marshal.ReleaseComObject(recipientCC);
                if (recipientTo != null) Marshal.ReleaseComObject(recipientTo);
                if (recipients != null) Marshal.ReleaseComObject(recipients);
            }
            return retValue;
        }

        //Fetches the HTML Email body template based on the environment nd then formats it.
        public static string GetMsPatchingEmailBody(DateTime patchdate, string environment)
        {
            string body = string.Empty;
            string templatePath = string.Empty;
            string actualPatchDate = patchdate.Date.ToString("D", new CultureInfo("en-GB")).Replace(patchdate.DayOfWeek.ToString(), "");
            string startPatchDate = patchdate.AddDays(-2).Date.ToString("D", new CultureInfo("en-GB")).Replace(patchdate.DayOfWeek.ToString(), "");
            string endPatchDate = patchdate.AddDays(1).Date.ToString("D", new CultureInfo("en-GB")).Replace(patchdate.DayOfWeek.ToString(), "");
            switch (environment)
            {
                case Constants.envDev3Test3:
                    templatePath = Path.GetFullPath(ConfigurationManager.AppSettings["Dev3Test3TemplatePath"]);
                    break;
                case Constants.envUAT:
                    templatePath = Path.GetFullPath(ConfigurationManager.AppSettings["UATTemplatePath"]);
                    break;
                case Constants.envProdDev2Test2:
                    templatePath = Path.GetFullPath(ConfigurationManager.AppSettings["ProdDev2Test2TemplatePath"]);
                    break;
                default:
                    return string.Empty;
            }
            if (!string.IsNullOrEmpty(templatePath))
            {
                using (StreamReader reader = new StreamReader(templatePath))
                {
                    body = reader.ReadToEnd();
                    //Replace teh date placeholders in the email body
                    body = string.Format(body, actualPatchDate, startPatchDate, endPatchDate);
                }
            }

            return body;
        }

        //Fetches the email subject based on the environment nd then formats it.
        public static string GetPatchingEmailSubject(DateTime patchdate, string environment)
        {
            string mailSubject = string.Empty;
            string actualPatchDate = patchdate.Date.ToString("D", new CultureInfo("en-GB")).Replace(patchdate.DayOfWeek.ToString(), "");
            string startPatchDate = patchdate.AddDays(-2).Date.ToString("D", new CultureInfo("en-GB")).Replace(patchdate.DayOfWeek.ToString(), "");
            string endPatchDate = patchdate.AddDays(1).Date.ToString("D", new CultureInfo("en-GB")).Replace(patchdate.DayOfWeek.ToString(), "");
            switch (environment)
            {
                case Constants.envDev3Test3:
                    {
                        mailSubject = String.Format(ConfigurationManager.AppSettings["Dev3Test3MailSubject"].ToString(), actualPatchDate);
                        break;
                    }
                case Constants.envUAT:
                    {
                        mailSubject = String.Format(ConfigurationManager.AppSettings["UATMailSubject"], startPatchDate, endPatchDate);
                        break;
                    }
                case Constants.envProdDev2Test2:
                    {
                        mailSubject = String.Format(ConfigurationManager.AppSettings["ProdDev2Test2MailSubject"], actualPatchDate);
                        break;
                    }
                default:
                    return string.Empty;
            }
            return mailSubject;
        }

        //Retrieves a list of recipients email ids from the respective distribution files
        public static List<string> GetRecipientsList(string recipientListfilePath)
        {
            List<string> recipientsListFile = Enumerable.Empty<string>().ToList();

            using (StreamReader fileReader = new StreamReader(File.OpenRead(recipientListfilePath)))
            {
                List<string> nonDuplicateList = RemoveDuplicateEmailIds(fileReader); //remove duplicate email ids, if any
                if (nonDuplicateList.Any() && nonDuplicateList.Count > 0)
                {
                    foreach (string emailid in nonDuplicateList)
                    {
                        if (isValidEmail(emailid))
                        {
                            recipientsListFile.Add(emailid);
                        }
                        else
                        {
                            Log.write("Email id-->" + emailid + "in teh distribution list is invalid so not excluded from recipient list", null, MsPatchingLogPath);
                        }
                    }
                }
            }
            return recipientsListFile;
        }
        //validates the email ids specified in the To/CC distribution files 
        public static bool isValidEmail(string emailid)
        {
            bool isValid = false;
            try
            {
                if (!string.IsNullOrEmpty(emailid))
                {
                    isValid = Regex.IsMatch(emailid, MatchEmailPattern);
                }
                else
                {
                    isValid = false;
                }
            }
            catch (RegexMatchTimeoutException ex)
            {
                Log.write("RegexMatchTimeoutException exception in the isValidEmail method", ex, MsPatchingLogPath);
            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception in the isValidEmail method", ex, MsPatchingLogPath);
            }
            return isValid;
        }

        //Removes the Duplicate email ids from distribution list, if any.
        public static List<string> RemoveDuplicateEmailIds(StreamReader fileReader)
        {
            List<string> nonDuplicateEmailList = new List<string>();
            var lines = new HashSet<int>();
            try
            {
                while (!fileReader.EndOfStream)
                {
                    string line = fileReader.ReadLine().Trim();
                    int hc = line.GetHashCode();
                    if (lines.Contains(hc))
                        continue;

                    lines.Add(hc);
                    nonDuplicateEmailList.Add(line);
                }
            }
            catch (ObjectDisposedException ex)
            {
                Log.write("ObjectDisposedException exception in the RemoveDuplicateEmailIds method", ex, MsPatchingLogPath);
                fileReader.Close();
                return Enumerable.Empty<string>().ToList();
            }
            catch (OutOfMemoryException ex)
            {
                Log.write("OutOfMemoryException exception in the RemoveDuplicateEmailIds method", ex, MsPatchingLogPath);
                fileReader.Close();
                return Enumerable.Empty<string>().ToList();
            }
            catch (IOException ex)
            {
                Log.write("IOException exception in the RemoveDuplicateEmailIds method", ex, MsPatchingLogPath);
                fileReader.Close();
                return Enumerable.Empty<string>().ToList();
            }
            fileReader.Close();
            return nonDuplicateEmailList;
        }

        //Fetches the Partial email body from HTML template and forms the complete email body for MS Nuget 
        public static string GetNugetPatchingEmailBody()
        {
            string templateBody = string.Empty;
            string emailBody = string.Empty;
            string tablestart = "<table rules='all' style='border:black solid 1px;width:40%;text-align:center;' cellpadding= 0 cellspacing=0><thead style='font-weight: bold;'><tr><td style='border-right:black solid 1px;border-bottom:black solid 1px;background-color:lightgray;'>Solution</td><td style ='border-bottom:black solid 1px;background-color:lightgray;'>SPOCS</td></tr></thead><tbody>";

            try
            {
                Url NugetDllExcelPath = new Url(ConfigurationManager.AppSettings["NugetSpreadsheetUrl"]);
                if (!string.IsNullOrEmpty(NugetDllExcelPath.ToString()))
                {
                    List<SolutionSpocs> lstSolSpocs = GetSolutionWiseSpocsList();
                    string templatePath = Path.GetFullPath(ConfigurationManager.AppSettings["NugetTemplatePath"]);

                    if (!string.IsNullOrEmpty(templatePath) && lstSolSpocs != null)
                    {
                        using (StreamReader reader = new StreamReader(templatePath))
                        {
                            templateBody = reader.ReadToEnd();
                            StringBuilder solSpocTable = new StringBuilder();
                            solSpocTable.Append(tablestart);
                            foreach (SolutionSpocs solSpocs in lstSolSpocs)
                            {
                                solSpocTable.Append("<tr><td style='border-right: black solid 1px;border-bottom: black solid 1px;'>" + solSpocs.SolutionName + "</td><td style='border-bottom: black solid 1px;'>" + solSpocs.SpocName + "</td></tr>");
                            }
                            solSpocTable.Append("</tbody></table>");
                            //Replace teh date placeholders in the email body
                            emailBody = templateBody.Replace("{0}", NugetDllExcelPath.ToString()).Replace("{1}", solSpocTable.ToString());
                        }
                    }
                }
            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception in the GetNugetPatchingEmailBody method, thus email body is empty.", ex, NugetPatchingLogPath);
                return string.Empty;
            }
            catch (System.Security.SecurityException ex)
            {
                Log.write("SecurityException exception in the GetNugetPatchingEmailBody method, thus email body is empty.", ex, NugetPatchingLogPath);
                return string.Empty;
            }
            catch (NotSupportedException ex)
            {
                Log.write("NotSupportedException exception in the GetNugetPatchingEmailBody method, thus email body is empty.", ex, NugetPatchingLogPath);
                return string.Empty;
            }
            catch (PathTooLongException ex)
            {
                Log.write("PathTooLongException exception in the GetNugetPatchingEmailBody method, thus email body is empty.", ex, NugetPatchingLogPath);
                return string.Empty;
            }
            catch (FileNotFoundException ex)
            {
                Log.write("FileNotFoundException exception in the GetNugetPatchingEmailBody method, thus email body is empty.", ex, NugetPatchingLogPath);
                return string.Empty;
            }
            catch (DirectoryNotFoundException ex)
            {
                Log.write("DirectoryNotFoundException exception in the GetNugetPatchingEmailBody method, thus email body is empty.", ex, NugetPatchingLogPath);
                return string.Empty;
            }

            return emailBody;
        }
        public static List<SolutionSpocs> GetSolutionWiseSpocsList()
        {
            string solutionSpocsPath = Path.GetFullPath(ConfigurationManager.AppSettings["SolutionSpocsListFile"]);
            List<SolutionSpocs> SolutionWiseSpocslist = Enumerable.Empty<SolutionSpocs>().ToList();

            using (StreamReader fileReader = new StreamReader(File.OpenRead(solutionSpocsPath)))
            {
                var lines = new HashSet<int>();
                try
                {
                    while (!fileReader.EndOfStream)
                    {
                        string line = fileReader.ReadLine().Trim();
                        int hc = line.GetHashCode();
                        if (lines.Contains(hc))
                            continue;
                        lines.Add(hc);
                        string[] splittedList = line.Split('-');
                        SolutionWiseSpocslist.Add(new SolutionSpocs(splittedList[0], splittedList[1]));
                    }
                }
                catch (ObjectDisposedException ex)
                {
                    Log.write("ObjectDisposedException exception in the GetSolutionWiseSpocsList method", ex, MsPatchingLogPath);
                    fileReader.Close();
                    return Enumerable.Empty<SolutionSpocs>().ToList();
                }
                catch (OutOfMemoryException ex)
                {
                    Log.write("OutOfMemoryException exception in the GetSolutionWiseSpocsList method", ex, MsPatchingLogPath);
                    fileReader.Close();
                    return Enumerable.Empty<SolutionSpocs>().ToList();
                }
                catch (IOException ex)
                {
                    Log.write("IOException exception in the GetSolutionWiseSpocsList method", ex, MsPatchingLogPath);
                    fileReader.Close();
                    return Enumerable.Empty<SolutionSpocs>().ToList();
                }
            }
            return SolutionWiseSpocslist;
        }
        public static string GetNugetPatchingEmailSubject(DateTime patchdate)
        {
            string mailSubject = string.Empty;
            string formattedPatchDate = patchdate.Date.ToString("D", new CultureInfo("en-GB")).Replace(patchdate.DayOfWeek.ToString(), "");
            string configSubject = ConfigurationManager.AppSettings["NugetMailSubject"].ToString();

            if (!string.IsNullOrEmpty(configSubject))
            {
                mailSubject = String.Format(configSubject, formattedPatchDate);
            }

            return mailSubject;
        }

        public static string GetCCBEmailBody(DateTime CCBReadyDate)
        {

            string templateBody = string.Empty;
            string emailBody = string.Empty;
            DateTime CCBReadyDay = CCBReadyDate.AddDays(1);
            try
            {

                string templatePath = Path.GetFullPath(ConfigurationManager.AppSettings["CCBEmailTemplatePath"]);

                if (!string.IsNullOrEmpty(templatePath))
                {
                    using (StreamReader reader = new StreamReader(templatePath))
                    {
                        templateBody = reader.ReadToEnd();


                        //Replace teh date placeholders in the email body
                        //emailBody = string.Format(templateBody, CCBReadyDay.ToString("D", new CultureInfo("en-GB")).Replace(CCBReadyDay.DayOfWeek.ToString(),"");
                        //emailBody=string.Format(templateBody,CCBReadyDay.ToString("D", new CultureInfo("en-GB")).Replace(CCBReadyDay.DayOfWeek.ToString(),""));
                        emailBody = templateBody.Replace("{0}", CCBReadyDay.ToString("D", new CultureInfo("en-GB")).Replace(CCBReadyDay.DayOfWeek.ToString(), ""));
                    }
                }
            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception in the GetCCBEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (System.Security.SecurityException ex)
            {
                Log.write("SecurityException exception in the GetCCBEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (NotSupportedException ex)
            {
                Log.write("NotSupportedException exception in the GetCCBEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (PathTooLongException ex)
            {
                Log.write("PathTooLongException exception in the GetCCBEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (FileNotFoundException ex)
            {
                Log.write("FileNotFoundException exception in the GetCCBEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (DirectoryNotFoundException ex)
            {
                Log.write("DirectoryNotFoundException exception in the GetCCBEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }

            return emailBody;
        }
        public static string GetCCBEmailSubject(DateTime CCBday)
        {
            string mailSubject = string.Empty;
            DateTime CCBMonday = CCBday.AddDays(5);
            string formattedPatchDate = CCBMonday.Date.ToString("D", new CultureInfo("en-GB")).Replace(CCBMonday.DayOfWeek.ToString(), "");
            string configSubject = ConfigurationManager.AppSettings["CCBMailSubject"].ToString();

            if (!string.IsNullOrEmpty(configSubject))
            {
                mailSubject = String.Format(configSubject, formattedPatchDate);
            }

            return mailSubject;
        }
        public static string GetCCBPublishEmailBody(DateTime CCBPublishDate)
        {

            string templateBody = string.Empty;
            string emailBody = string.Empty;
            DateTime CCBReadyDay = CCBPublishDate.AddDays(3);
            try
            {

                string templatePath = Path.GetFullPath(ConfigurationManager.AppSettings["CCBEmailPublishTemplatePath"]);

                if (!string.IsNullOrEmpty(templatePath))
                {
                    using (StreamReader reader = new StreamReader(templatePath))
                    {
                        templateBody = reader.ReadToEnd();

                        //Replace teh date placeholders in the email body
                        emailBody = templateBody.Replace("{0}", CCBReadyDay.ToString("D", new CultureInfo("en-GB")).Replace(CCBReadyDay.DayOfWeek.ToString(), ""));
                    }
                }

            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception in the GetCCBPublishEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (System.Security.SecurityException ex)
            {
                Log.write("SecurityException exception in the GetCCBPublishEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (NotSupportedException ex)
            {
                Log.write("NotSupportedException exception in the GetCCBPublishEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (PathTooLongException ex)
            {
                Log.write("PathTooLongException exception in the GetCCBPublishEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (FileNotFoundException ex)
            {
                Log.write("FileNotFoundException exception in the GetCCBPublishEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }
            catch (DirectoryNotFoundException ex)
            {
                Log.write("DirectoryNotFoundException exception in the GetCCBPublishEmailBody method, thus email body is empty.", ex, CCBMailLogPath);
                return string.Empty;
            }

            return emailBody;
        }
        public static string GetCCBPublishEmailSubject(DateTime CCBPublishday)
        {
            string mailSubject = string.Empty;
            DateTime CCBFriday = CCBPublishday.AddDays(3);
            string formattedPatchDate = CCBFriday.Date.ToString("D", new CultureInfo("en-GB")).Replace(CCBFriday.DayOfWeek.ToString(), "");
            string configSubject = ConfigurationManager.AppSettings["CCBMailPublishSubject"].ToString();

            if (!string.IsNullOrEmpty(configSubject))
            {
                mailSubject = String.Format(configSubject, formattedPatchDate);
            }

            return mailSubject;
        }

    }
}
