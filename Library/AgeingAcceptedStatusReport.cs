﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Library
{
    public class AgeingAcceptedStatusReport
    {
        private readonly List<string> TicketStatusList = new List<string>(new string[] { "< 7 days", "7 days to 14 days", "14 days to 30 days", ">= 30 days" });
        private DataTable dtAgeingAcceptedReport;

        public void GenerateAgeingAcceptedStatusReport(DataTable dtSolutions, DataTable dtExcel, out DataTable dtAgeingAcceptedIncidnet, out DataTable dtAgeingAcceptedServiceRequets, out DataTable dtAgeingAcceptedProblem)
        {
            dtAgeingAcceptedIncidnet = dtAgeingAcceptedServiceRequets = dtAgeingAcceptedProblem = new DataTable();
            dtSolutions.DefaultView.Sort = "Other Products";
            DataTable temp = dtSolutions.DefaultView.ToTable(true, "Other Products");
            dtAgeingAcceptedReport = new DataTable();
            dtAgeingAcceptedReport.Columns.Add("Solution");
            dtAgeingAcceptedReport.Columns.Add("< 7 days");
            dtAgeingAcceptedReport.Columns.Add("7 days to 14 days");
            dtAgeingAcceptedReport.Columns.Add("14 days to 30 days");
            dtAgeingAcceptedReport.Columns.Add(">= 30 days");
            dtAgeingAcceptedReport.Columns.Add("Grand Total", typeof(Int32));
            DataRow datarow;
            string ticketType = "";
            for (int j = 0; j < 3; j++)
            {
                if (j == 0)
                    ticketType = "Incident";
                if (j == 1)
                    ticketType = "ServiceReq";
                if (j == 2)
                    ticketType = "Problem";

                foreach (DataRow dr in temp.Rows)
                {
                    int counter = 0;
                    int rowTotal = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[0])))
                        continue;
                    datarow = dtAgeingAcceptedReport.NewRow();
                    datarow[counter] = dr.ItemArray[0].ToString();
                    foreach (var data in TicketStatusList)
                    {
                        counter++;
                        DataRow[] result = null;
                        if (ticketType == "ServiceReq")
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Status] = 'Accepted' and [AgeGroup] = '" + data + "' and [Parent Object] = '" + ticketType + "'");
                        else
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Status] = 'Accepted' and [AgeGroup] = '" + data + "' and [Parent Object] = '" + ticketType + "'");
                        datarow[counter] = (result.Any()) ? result.Count() : 0;
                        rowTotal += Convert.ToInt32(datarow[counter].ToString());

                    }
                    datarow[counter + 1] = rowTotal;

                    if (rowTotal != 0)
                        dtAgeingAcceptedReport.Rows.Add(datarow);
                }

                //sort by total
                DataView dataview = dtAgeingAcceptedReport.DefaultView;
                dataview.Sort = "[Grand Total] DESC";
                dtAgeingAcceptedReport = dataview.ToTable();

                //add column total row
                datarow = dtAgeingAcceptedReport.NewRow();
                dtAgeingAcceptedReport.Rows.Add(datarow);
                datarow[0] = "Grand Total";

                for (int i = 1; i < dtAgeingAcceptedReport.Columns.Count; i++)
                {
                    datarow[i] = 0;
                    datarow[i] = dtAgeingAcceptedReport.AsEnumerable().Sum(x => Convert.ToInt32(x[dtAgeingAcceptedReport.Columns[i].ColumnName]));
                }

                if (dtAgeingAcceptedReport.Rows.Count > 0)
                {
                    if (j == 0)
                        dtAgeingAcceptedIncidnet = dtAgeingAcceptedReport.Copy();
                    if (j == 1)
                        dtAgeingAcceptedServiceRequets = dtAgeingAcceptedReport.Copy();
                    if (j == 2)
                        dtAgeingAcceptedProblem = dtAgeingAcceptedReport.Copy();
                }

                dtAgeingAcceptedReport.Clear();
            }
        }


    }
}
