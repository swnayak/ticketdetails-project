﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class CCBMailData
    {
        public string logPath;
        public void Process()
        {
            Email.CCBMailLogPath = logPath;
            DateTime todayDate = DateTime.Now;

            //Check if it is wednesday of the week
            if (todayDate.DayOfWeek == DayOfWeek.Wednesday)
            {
                Email.SendPatchingEmail(todayDate, string.Empty, Constants.CCBMail, Email.CCBMailLogPath);
            }
        }
    }
}
