﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class NugetPatchingData
    {
        public string logPath;
        public void Process()
        {
            Email.NugetPatchingLogPath = logPath;
            DateTime todayDate = DateTime.Now;

            //Check if it is first friday of the month
            bool isFirstFridayOfMonth = (todayDate.DayOfWeek == DayOfWeek.Friday && todayDate.Day <= 7);
            if (isFirstFridayOfMonth)
            {
                Email.SendPatchingEmail(todayDate,string.Empty,Constants.nugetpatching,Email.NugetPatchingLogPath);
            }
        }
    }
}
