﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Library
{
    public class OverAllStatusReport
    {
        private readonly List<string> TicketStatusList = new List<string>(new string[] {"Assigned", "Reassigned", "Accepted", "Waiting", "Logged"});
        private DataTable dtActiveStatusReport;
        public void GenerateOverAllStatusReport(DataTable dtSolutions, DataTable dtExcel, out DataTable dtOverAllActiveIncidnet, out DataTable dtOverAllActiveServiceRequets, out DataTable dtOverAllActiveProblem)
        {
            dtOverAllActiveIncidnet = dtOverAllActiveServiceRequets = dtOverAllActiveProblem = new DataTable();
            dtSolutions.DefaultView.Sort = "Other Products";
            DataTable temp = dtSolutions.DefaultView.ToTable(true, "Other Products");
            dtActiveStatusReport = new DataTable();
            dtActiveStatusReport.Columns.Add("Service-Solution");
            dtActiveStatusReport.Columns.Add("Assigned");
            dtActiveStatusReport.Columns.Add("Reassigned");
            dtActiveStatusReport.Columns.Add("Accepted");
            dtActiveStatusReport.Columns.Add("Waiting");
            dtActiveStatusReport.Columns.Add("Logged");
            dtActiveStatusReport.Columns.Add("Grand Total", typeof(Int32));
            DataRow datarow;
            string ticketType = "";
            for (int j = 0; j < 3; j++)
            {
                if (j == 0)
                    ticketType = "Incident";
                if (j == 1)
                    ticketType = "ServiceReq";
                if (j == 2)
                    ticketType = "Problem";

                foreach (DataRow dr in temp.Rows)
                {
                    int counter = 0;
                    int rowTotal = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[0])))
                        continue;
                    datarow = dtActiveStatusReport.NewRow();
                    datarow[counter] = dr.ItemArray[0].ToString();
                    foreach (var data in TicketStatusList)
                    {
                        counter++;
                        DataRow[] result = null;
                        if (ticketType == "ServiceReq")
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Status] = '" + data + "' and [Parent Object] = '" + ticketType + "'");
                        else
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Status] = '" + data + "' and [Parent Object] = '" + ticketType + "'");

                        datarow[counter] = (result.Any()) ? result.Count() : 0;
                        rowTotal += Convert.ToInt32(datarow[counter].ToString());

                    }
                    datarow[counter + 1] = rowTotal;

                    if (rowTotal != 0)
                        dtActiveStatusReport.Rows.Add(datarow);
                }


                //sort by total
                DataView dataview = dtActiveStatusReport.DefaultView;
                dataview.Sort = "[Grand Total] DESC";
                dtActiveStatusReport = dataview.ToTable();

                //add column total row
                datarow = dtActiveStatusReport.NewRow();
                dtActiveStatusReport.Rows.Add(datarow);
                datarow[0] = "Grand Total";

                for (int i = 1; i < dtActiveStatusReport.Columns.Count; i++)
                {
                    datarow[i] = 0;
                    datarow[i] = dtActiveStatusReport.AsEnumerable().Sum(x => Convert.ToInt32(x[dtActiveStatusReport.Columns[i].ColumnName]));
                }

                if (dtActiveStatusReport.Rows.Count > 0)
                {
                    if (j == 0)
                        dtOverAllActiveIncidnet = dtActiveStatusReport.Copy();
                    if (j == 1)
                        dtOverAllActiveServiceRequets = dtActiveStatusReport.Copy();
                    if (j == 2)
                        dtOverAllActiveProblem = dtActiveStatusReport.Copy();
                }
                dtActiveStatusReport.Clear();
            }
        }
    }
}
