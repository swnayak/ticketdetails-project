﻿using Library;
using System;
using System.Configuration;
using System.IO;
using System.Linq;


namespace TicketDetails
{
    class Program
    {
        static void Main(string[] args)
        {
            string logPath = ConfigurationManager.AppSettings["Log"].ToString();
            try
            {
                Log.write("========================================================================", null, logPath);
                Log.write("Job started at: " + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"), null, logPath);

                string conStr = ConfigurationManager.AppSettings["XlsxConn"].ToString();
                string directory = ConfigurationManager.AppSettings["FilePath"].ToString();
                string masterDirectory = ConfigurationManager.AppSettings["MasterExelPath"].ToString();
                string archiveDir = ConfigurationManager.AppSettings["ArchivePath"].ToString();                        
                Helper.ExecuteExcelFile(conStr, directory, masterDirectory);
                Helper.ArchiveFile(directory, archiveDir);

                Log.write("Job ends at: " + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"), null, logPath);
                Log.write("========================================================================", null, logPath);
            }
            catch(Exception ex)
            {
                Log.write("ERROR: ", ex, logPath);
            }
        }           
    }
}



