﻿using System;
using System.IO;
using Library;
using System.Configuration;

namespace CCBMail
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string logPath = Path.GetFullPath(ConfigurationManager.AppSettings["Log"].ToString());
                //string logPath = @"D:\ganesh\sca_internal\Logs\Log.txt";
                Log.write("/*****************CCB Mail triggered starts at " + DateTime.Now.ToString() + "*****************/", null, logPath);

                CCBMailHelper.logPath = logPath;
                CCBMailHelper.ExecuteCCBReminders();

                Log.write("/*****************CCB Mail triggered ends at " + DateTime.Now.ToString() + "*****************/", null, logPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
