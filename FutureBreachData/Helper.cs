﻿using Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FutureBreachData
{
    public class Helper
    {
        static string logPath = ConfigurationManager.AppSettings["Log"].ToString();

        public static void ExecuteExcelFile(string conStr, string directory)
        {
            try
            {
                string[] files;                
                string connectionString = "";               

                if (!Directory.Exists(directory))
                {
                    Log.write("Directory/excel files does not exist", null, logPath);
                    return;
                }
                files = Directory.GetFiles(directory, @"*.xls", SearchOption.TopDirectoryOnly);                
                if (files.Count() == 1)
                    connectionString = string.Format(conStr, files[0]);                

                BreachDetails objBreachDetails = new BreachDetails(connectionString);
                //setup mail details
                objBreachDetails.mailTo = ConfigurationManager.AppSettings["MailTo"].ToString();
                objBreachDetails.mailCC = ConfigurationManager.AppSettings["MailCC"].ToString();
                objBreachDetails.futureBreachMailSubject = ConfigurationManager.AppSettings["FutureBreachMailSubject"].ToString();               
                objBreachDetails.logPath = logPath;
                //Setup master records
                Log.write("Loading master data", null, logPath);
                objBreachDetails.LoadMasterData();

                //start processing
                Log.write("Processing of the excel file started", null, logPath);
                objBreachDetails.Process();
            }
            catch (Exception ex)
            {
                Log.write("ERROR: ", ex, logPath);
            }
        }

        internal static void ArchiveFile(string directory, string archiveDir)
        {
            try
            {
                Log.write("Archiving of the excel file started", null, logPath);
                if (System.IO.Directory.Exists(directory))
                {
                    string[] files = System.IO.Directory.GetFiles(directory);

                    foreach (string s in files)
                    {
                        string fileName = Path.GetFileName(s);
                        string destFile = Path.Combine(archiveDir + "\\", Guid.NewGuid().ToString() + "_" + fileName);
                        File.Move(s, destFile);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.write("ERROR: ", ex, logPath);
            }


        }
    }
}
