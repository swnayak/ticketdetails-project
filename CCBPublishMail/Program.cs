﻿
using Library;
using System;
using System.Configuration;
using System.IO;

namespace CCBPublishMail
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello");

            string logPath = Path.GetFullPath(ConfigurationManager.AppSettings["Log"].ToString());
            //string logPath = @"D:\ganesh\sca_internal\Logs\Log.txt";
            Log.write("/*****************CCB Mail triggered starts at " + DateTime.Now.ToString() + "*****************/", null, logPath);

            CCBPublishMailHelper.logPath = logPath;
            CCBPublishMailHelper.ExecuteCCBReminders();

            Log.write("/*****************CCB Mail triggered ends at " + DateTime.Now.ToString() + "*****************/", null, logPath);
        }
    }
}
